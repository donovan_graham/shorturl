import { isValidUrl } from "../src/validation";

describe("isValidUrl", () => {
  const validUrls = [
    "http://foo.com",
    "https://foo.com/",
    "http://foo.com/blah_blah/",
    "http://www.example.com/wpstyle/?p=364",
    "https://www.example.com/foo/?bar=baz&inga=42&quux",
    "http://142.42.1.1/",
    "http://142.42.1.1:8080",
  ];

  test.each(validUrls)('valid url: "%s" ', (url) => {
    expect(isValidUrl(url)).toBe(true);
  });

  const invalidUrls = [
    "file://some/file",
    "data://2i4y23i",
    "http://",
    "http://.",
    "http://..",
    "http://../",
    "http://?",
    "http://??",
    "http://??/",
    "http://#",
    "http://##",
    "http://##/",
    "//",
    "//a",
    "///a",
    "///",
    "foo.com",
  ];

  test.each(invalidUrls)('invalid url: "%s" ', (url) => {
    expect(isValidUrl(url)).toBe(false);
  });
});
