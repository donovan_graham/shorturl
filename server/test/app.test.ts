import request from "supertest";
import * as storage from "../src/storage";

jest.mock("../src/storage", () => ({
  connect: () => "OK",
  getCounter: () => 1_234_567_890,
  setUrlWithSlug: () => "OK",
  getUrlBySlug: (slug: string) =>
    slug === "valid" ? "http://some.url/to/redirect/to" : "",
}));

import app from "../src/app";

const agent = request.agent(app);

describe("Integration", () => {
  describe("Health", () => {
    it("returns 200 OK", async () => {
      const response = await agent.get("/v1/api/health");
      expect(response.status).toBe(200);
      expect(response.text).toBe("OK");
    });
  });

  describe("Generate", () => {
    it("returns 200 payload", async () => {
      const response = await agent
        .post("/v1/api/generate")
        .send({ url: "http://some.url/to/work/with" });

      expect(response.status).toBe(200);
      expect(response.body).toEqual({
        slug: "BjRjfe",
        shortUrl: "http://localhost:8080/BjRjfe",
      });
    });
  });

  describe("ShortUrl", () => {
    it("returns 302 https://some.url/ for valid slug", async () => {
      const response = await agent.get("/valid");
      expect(response.status).toEqual(302);
      expect(response.header.location).toBe("http://some.url/to/redirect/to");
    });

    it("returns 302 / for invalid slug", async () => {
      const response = await agent.get("/invalid");
      expect(response.status).toBe(302);
      expect(response.header.location).toBe("/");
    });
  });
});
