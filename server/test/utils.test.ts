import { encode } from "../src/utils";

describe("encode", () => {
  const numbers = [
    [0, ""],
    [1, "B"],
    [230, "Dz"],
    [10_000_000, "vTvp"],
    [213_123_321_123, "EiErCgD"],
  ];

  test.each(numbers)('encode: "%i" ', (number: number, slug: string) => {
    expect(encode(number)).toBe(slug);
  });
});
