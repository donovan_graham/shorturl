import { connect, seed } from "./storage";
import app from "./app";

// start server
const server = app.listen(3000, async () => {
  console.log("Server is running");
  connect();
  try {
    await seed(100_000_000); // lets start on something interesting :)
  } catch (e) {
    console.error(e);
  }
});

export default server;
