import Redis from "ioredis";
import { REDIS_SERVICE_HOST, REDIS_SERVICE_PORT } from "./config";

const COUNTER_KEY = "counter";

let redis: Redis.Redis;

export function connect(): void {
  redis = new Redis({
    host: REDIS_SERVICE_HOST,
    port: REDIS_SERVICE_PORT,
    maxRetriesPerRequest: 3,
  });
  redis.on("error", console.dir);
}

export async function seed(value: number): Promise<void> {
  await redis.setnx(COUNTER_KEY, value);
}

export async function getCounter(): Promise<number> {
  return await redis.incr(COUNTER_KEY);
}

export async function setUrlWithSlug(slug: string, url: string): Promise<void> {
  await redis.set(slug, url);
}

export async function getUrlBySlug(slug: string): Promise<string> {
  return await redis.get(slug);
}
