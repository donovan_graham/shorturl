import express, { NextFunction, Request, Response } from "express";
import path from "path";
import helmet from "helmet";

import { healthCheckRoute, generateSlugRoute, shortUrlRoute } from "./routes";

const assetPath = path.join(__dirname, "app");
const indexHtmlPath = path.join(assetPath, "index.html");
const staticPath = path.join(__dirname, "static");

const app = express();
app.set("trust proxy", true);

// security configuration
// app.use(helmet());
// ...is equivalent to this:
app.use(helmet.contentSecurityPolicy());
app.use(helmet.dnsPrefetchControl());
app.use(helmet.expectCt());
app.use(helmet.frameguard());
app.use(helmet.hidePoweredBy());
// app.use(helmet.hsts());
app.use(helmet.ieNoOpen());
app.use(helmet.noSniff());
app.use(helmet.permittedCrossDomainPolicies());
app.use(helmet.referrerPolicy());
app.use(helmet.xssFilter());

// parse json body
app.use(express.json());

// static assets
app.use("/v1/assets", express.static(assetPath));
app.use("/v1/static", express.static(staticPath));

// routes
app[healthCheckRoute.method](healthCheckRoute.path, healthCheckRoute.handler);
app[generateSlugRoute.method](
  generateSlugRoute.path,
  generateSlugRoute.handler
);
app[shortUrlRoute.method](shortUrlRoute.path, shortUrlRoute.handler);

// index
app.get("/", (req: Request, res: Response) => res.sendFile(indexHtmlPath));
app.get("*", (req: Request, res: Response) => res.redirect("/"));

// error handler
app.use((err: Error, req: Request, res: Response, next: NextFunction) =>
  res.status(500).send("An error occurred")
);

export default app;
