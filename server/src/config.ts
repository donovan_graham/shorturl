export const REDIS_SERVICE_HOST: string =
  process.env.REDIS_SERVICE_HOST || "redis";
export const REDIS_SERVICE_PORT: number = parseInt(
  process.env.REDIS_SERVICE_PORT || "6379"
);
export const APPLICATION_PUBLIC_URL: string =
  process.env.APPLICATION_PUBLIC_URL;

if (!APPLICATION_PUBLIC_URL) {
  throw new Error('Environment variable "APPLICATION_PUBLIC_URL" is missing');
}
