import { Request, Response, RequestHandler } from "express";
import asyncHandler from "express-async-handler";

import { APPLICATION_PUBLIC_URL } from "./config";
import { encode, characters } from "./utils";
import { isValidUrl } from "./validation";
import { getCounter, setUrlWithSlug, getUrlBySlug } from "./storage";

enum HttpMethod {
  GET = "get",
  POST = "post",
}

interface Route {
  method: HttpMethod;
  path: string;
  handler: RequestHandler;
}

const apiBasePath = "/v1/api";

const healthCheckPath = `${apiBasePath}/health`;

const healthCheckHandler = (req: Request, res: Response) => res.send("OK");

export const healthCheckRoute: Route = {
  method: HttpMethod.GET,
  path: healthCheckPath,
  handler: asyncHandler(healthCheckHandler),
};

const generateSlugPath = `${apiBasePath}/generate`;

const generateSlugHandler = async (req: Request, res: Response) => {
  const url = req.body.url;
  if (!url || !isValidUrl) throw new Error("invalid url");

  const number = await getCounter();
  const slug = encode(number);
  await setUrlWithSlug(slug, url);
  const shortUrl = `${APPLICATION_PUBLIC_URL}/${slug}`;
  return res.json({ slug, shortUrl });
};

export const generateSlugRoute: Route = {
  method: HttpMethod.POST,
  path: generateSlugPath,
  handler: asyncHandler(generateSlugHandler),
};

const escapedCharacters = characters.replace("-", "\\-");
const shortUrlPath = `/:slug([${escapedCharacters}]{1,12})`;

const shortUrlHandler = async (req: Request, res: Response) => {
  // slug is validated by express route pattern matching
  const slug = req.params.slug;
  const url = await getUrlBySlug(slug);

  if (!url) return res.redirect("/");
  return res.redirect(url);
};

export const shortUrlRoute: Route = {
  method: HttpMethod.GET,
  path: shortUrlPath,
  handler: asyncHandler(shortUrlHandler),
};
