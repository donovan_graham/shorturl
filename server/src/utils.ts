export const characters =
  "ABCDEFGHJKLMNPQRSTUVWXYZ_-abcdefghijkmnopqrstuvwxyz123456789";
const base = characters.length;

export const encode = (number: number): string => {
  let encoded = "";
  while (number) {
    const remainder = number % base;
    number = Math.floor(number / base);
    encoded = `${characters[remainder]}${encoded}`;
  }
  return encoded;
};
