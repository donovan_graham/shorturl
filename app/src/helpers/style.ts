export enum FontFamily {
  PacificoRegular = "Pacifico-Regular",
  RobotoRegular = "Roboto-Regular",
  RobotoMedium = "Roboto-Medium",
  RobotoBold = "Roboto-Bold",
}
