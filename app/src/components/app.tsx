import React from "react";
import styled from "styled-components";
import { FontFamily } from "../helpers/style";
import Form from "./form";

const SWrapper = styled.div`
  background-color: #ff6fa5;
  color: white;
  height: 100%;
  margin: 1rem;
  padding: 2rem;
  border-radius: 1rem;
  box-shadow: 0 2px 5px 1px #731348;
  min-width: 20rem;

  @media (max-width: 40rem) {
    border: none;
    box-shadow: none;
    margin: 0;
    border-radius: 0;
    padding: 2rem 1rem;
    background-color: #2a5bd7;
  }
`;

const SName = styled.h1`
  font-family: ${FontFamily.PacificoRegular};
  font-size: 3rem;
  line-height: auto;
  display: inline-block;
  margin-bottom: 2rem;
`;

const SFooter = styled.footer`
  font-family: ${FontFamily.PacificoRegular};
  margin-top: 2rem;
`;

const App: React.FC = () => (
  <SWrapper>
    <header>
      <SName>Shorty</SName>
    </header>
    <main>
      <Form />
    </main>
    <SFooter>&copy; 2021 Donovan Graham</SFooter>
  </SWrapper>
);

export default App;
