import React from "react";
import useAxios from "axios-hooks";
import styled from "styled-components";
import useSimpleForm, { FormValues, FormErrors } from "../hooks/simple-form";
import { isValidUrl } from "../helpers/validation";

enum FormFields {
  Url = "url",
}

const initialValues: FormValues = {
  [FormFields.Url]: "",
};

const validate = (values: FormValues): FormErrors => {
  const errors: FormErrors = {};
  if (!values[FormFields.Url] || !isValidUrl(values[FormFields.Url])) {
    errors[FormFields.Url] = "Invalid link to shorten";
  }
  return errors;
};

interface InputProps {
  readonly invalid: boolean;
}

const SInput = styled.input<InputProps>`
  width: 100%;
  height: 3rem;
  padding: 0.5rem;
  font-size: 1.5rem;
  line-height: 3rem;

  border: none;
  border-radius: 0.5rem;

  color: #515151;

  &::placeholder {
    color: pink;
  }

  @media (max-width: 40rem) {
    &::placeholder {
      color: #bdcbef;
    }
  }
`;

const SField = styled.div`
  display: flex;
`;

const SLabel = styled.label`
  display: none;
`;
const SHint = styled.p`
  display: none;
`;

const SActions = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 0.5rem;
`;

const SSubmitButton = styled.button`
  flex: 1;
  height: 3rem;
  font-size: 1.5rem;
  line-height: auto;
  margin-right: 0.5rem;

  border: 2px solid transparent;
  border-radius: 0.5rem;
  background-color: ivory;
  color: #8865ff;

  transition: background-color 120ms linear, border-color 120ms linear;

  &:hover,
  &:active,
  &:focus {
    border-color: #8865ff;
  }

  &:disabled {
    opacity: 0.4;
    border-color: transparent;
  }
`;

const SResetButton = styled.button`
  flex: 0 1 100px;
  height: 2rem;
  font-size: 1rem;
  line-height: 1.75rem;

  border: 2px solid transparent;
  border-radius: 0.5rem;
  background-color: ivory;
  color: #8865ff;

  transition: background-color 120ms linear, border-color 120ms linear;

  &:hover,
  &:active,
  &:focus {
    border-color: #8865ff;
  }

  &:disabled {
    opacity: 0.4;
    border-color: transparent;
  }
`;

const SResult = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: white;
  padding: 4rem 0;
  margin-top: 5rem;
  border-radius: 0.5rem;

  a {
    font-family: "Roboto-Bold";
    font-size: 2rem;
    color: #ec407a;

    @media (max-width: 40rem) {
      font-size: 1.5rem;
      color: #2a5bd7;
    }
  }
`;

const Form: React.FC = () => {
  const {
    values,
    errors,
    invalid,
    pristine,
    handleForm,
    handleFormReset,
    handleChange,
    handleBlur,
  } = useSimpleForm({ initialValues, validate, enableReinitialize: true });

  const [{ data, loading, error }, post] = useAxios(
    {
      url: "/v1/api/generate",
      method: "POST",
    },
    { manual: true }
  );

  const handleSubmit = (values: FormValues) => {
    post({ data: { url: values[FormFields.Url].trim() } });
  };

  const showResult = !!data || loading || !!error;

  return (
    <div>
      <form
        autoComplete="off"
        onSubmit={handleForm(handleSubmit)}
        onReset={handleFormReset}
        data-testid="form"
      >
        <SField role="url">
          <SLabel htmlFor="_url">Shorten you link</SLabel>
          <SInput
            id="_url"
            name={FormFields.Url}
            type="url"
            placeholder="Shorten your link"
            value={values[FormFields.Url]}
            invalid={!!errors[FormFields.Url]}
            disabled={loading}
            onChange={handleChange}
            onBlur={handleBlur}
            autoComplete="off"
            autoFocus={true}
            aria-invalid={!!errors[FormFields.Url]}
            aria-describedby="_urlHint"
            data-testid="url"
          />
          <SHint id="_urlHint">{errors[FormFields.Url]}</SHint>
        </SField>
        <SActions>
          <SSubmitButton
            type="submit"
            disabled={loading || pristine || invalid}
            data-testid="submit"
          >
            Shorten
          </SSubmitButton>
          <SResetButton
            type="reset"
            disabled={loading || !values[FormFields.Url]}
            data-testid="reset"
          >
            Start over
          </SResetButton>
        </SActions>
      </form>

      {showResult && (
        <SResult>
          {loading && <p>Loading...</p>}
          {error && <p>An error occurred, please retry.</p>}

          {data && (
            <a href={data.shortUrl} target="_blank" rel="noopener noreferrer">
              {data.shortUrl}
            </a>
          )}
        </SResult>
      )}
    </div>
  );
};

export default Form;
