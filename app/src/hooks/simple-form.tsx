import { useState, useEffect } from "react";
import get from "lodash/get";
import has from "lodash/has";
import isEqual from "lodash/isEqual";

const noop = () => ({});

export interface FormValues {
  [key: string]: string;
}

export interface FormErrors {
  [key: string]: string;
}

export interface FormVisited {
  [key: string]: boolean;
}

export interface SimpleForm {
  initialValues: FormValues;
  validate: (values: FormValues) => FormErrors;
  enableReinitialize: boolean;
}

const useSimpleForm = ({
  initialValues = {},
  validate = noop,
  enableReinitialize = false,
}: SimpleForm) => {
  const [prevValues, setPrevValues] = useState(initialValues);
  const [values, setValues] = useState<FormValues>(initialValues);
  const [errors, setErrors] = useState<FormErrors>(validate(initialValues));
  const [visited, setVisited] = useState<FormVisited>({});

  const valid = Object.keys(errors).length === 0;
  const invalid = !valid;
  const pristine = isEqual(prevValues, values);

  useEffect(() => {
    if (!enableReinitialize) return;
    if (isEqual(prevValues, initialValues)) return;

    setPrevValues(initialValues);
    setValues(initialValues);
    setErrors(validate(initialValues));
    setVisited({});
  }, [enableReinitialize, initialValues]);

  const setFieldValue = (name: string, value: string, runValidation = true) => {
    const newValues = { ...values, [name]: value };
    if (runValidation) {
      setErrors(validate(newValues));
    }
    setValues(newValues);
  };

  const setMultipleFieldValues = (
    newValues: FormValues,
    runValidation = true
  ) => {
    if (runValidation) {
      setErrors(validate(newValues));
    }

    setValues(newValues);
  };

  const setFieldVisited = (name: string) => {
    const newVisited = { ...visited, [name]: true };
    setVisited(newVisited);
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.persist();
    const {
      target: { name, type, checked, value },
    } = event;

    const fieldValue = type === "checkbox" ? checked.toString() : value;
    setFieldValue(name, fieldValue);
  };

  const handleBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    const {
      target: { name },
    } = event;
    setFieldVisited(name);
  };

  const handleForm = (handleSubmit: (cb: FormValues) => void) => (
    event: React.FormEvent<HTMLFormElement>
  ): void => {
    if (event) event.preventDefault();
    if (invalid) return;
    handleSubmit(values);
  };

  const handleFormReset = (event: React.FormEvent<HTMLFormElement>): void => {
    if (event) event.preventDefault();
    setValues(initialValues);

    setErrors(validate(initialValues));
    setVisited({});
  };

  const getField = (name: string) => ({
    value: get(values, name, ""),
    error: get(errors, name),
    visited: has(visited, name),
  });

  return {
    valid,
    invalid,
    pristine,
    handleChange,
    handleBlur,
    handleForm,
    validate,
    values,
    errors,
    visited,
    setValues,
    setErrors,
    setFieldValue,
    setMultipleFieldValues,
    setFieldVisited,
    handleFormReset,
    getField,
  };
};

export default useSimpleForm;
