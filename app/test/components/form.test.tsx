import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import * as useAxios from "axios-hooks";
import Form from "../../src/components/form";

jest.mock("axios-hooks");

describe("Component > Form", () => {
  let post: jest.Mock;
  let mockedAxios: jest.Mocked<typeof useAxios>;

  beforeEach(() => {
    post = jest.fn();

    mockedAxios = useAxios as jest.Mocked<typeof useAxios>;
    mockedAxios.default.mockReturnValue([
      { data: null, loading: false, error: undefined },
      post,
    ]);
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it("intial state", () => {
    const { container } = render(<Form />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it("type invalid url", () => {
    render(<Form />);
    const input = screen.getByTestId("url");

    userEvent.type(input, "file://from/disk");
    expect(input).toMatchSnapshot();
  });

  it("type valid url", () => {
    render(<Form />);
    const input = screen.getByTestId("url");

    userEvent.type(input, "http://some.com/long/url");
    expect(input).toMatchSnapshot();
  });

  it("reset form", () => {
    render(<Form />);
    const input = screen.getByTestId("url");
    const reset = screen.getByTestId("reset");

    userEvent.type(input, "http://some.com/long/url");
    userEvent.click(reset);
    expect(input).toMatchSnapshot();
  });

  it("unable to submit an invalid url", () => {
    render(<Form />);
    const input = screen.getByTestId("url");
    const submit = screen.getByTestId("submit");

    userEvent.type(input, "file://from/disk");
    userEvent.click(submit);
    expect(post).toBeCalledTimes(0);
  });

  it("able to submit a valid url", () => {
    render(<Form />);
    const input = screen.getByTestId("url");
    const submit = screen.getByTestId("submit");

    userEvent.type(input, "http://some.com/long/url");
    userEvent.click(submit);
    expect(post).toBeCalledTimes(1);
    expect(post).toHaveBeenCalledWith({
      data: { url: "http://some.com/long/url" },
    });
  });
});
