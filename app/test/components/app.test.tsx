import React from "react";
import { render } from "@testing-library/react";
import App from "../../src/components/app";

describe("Component > App", () => {
  it("intial state", () => {
    const { container } = render(<App />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
