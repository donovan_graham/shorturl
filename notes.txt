Requirement
===========
1. Docker + Docker-compose (version~3)
2. Git
3. Linux/unix based OS (tested)


Quick start (Development)
===========
In a new terminal tab run the following commands
$ git clone git@gitlab.com:donovan_graham/shorturl.git
$ ./setup.sh
$ docker-compose up
Wait for all services to be started and ready
=> navigate to browser http://localhost:8080


Running tests
=============
Run setup at least once, before continuing
$ ./setup.sh

Application test can be run as follows:
$ docker-compose run --rm --no-dep app npm run test

Server test can be run as follows:
$ docker-compose run --rm --no-dep server npm run test



Docker build (Production)
============
Run setup at least once, before continuing
$ ./setup.sh

To run docker build locally, run the following commands:
$ ./build.sh
$ docker-compose -f docker-compose.rc.yml up --remove-orphans
=> navigate to browser http://localhost:8080


Manual api test
---------------
Using curl you can manually test the api, you can run the following script against the above running production build
$ curl -X POST http://localhost:8080/v1/api/generate -d '{"url":"https://domain.com/very/long/url"}' -H "Content-Type: application/json"

Performance / Load test
-----------------------
If you have apache bench installed (ab), you can run the following script against the above running production build
$ ./benchmark.sh



Performance
===========

Tests on my local macbook (2 GHz Intel Core i7, 8 GB) with apache bench produced the following output

    Concurrency Level:      10
    Time taken for tests:   2.808 seconds
    Complete requests:      2000
    Failed requests:        0
    Total transferred:      1686000 bytes
    Total body sent:        386000
    HTML transferred:       114000 bytes
    Requests per second:    712.19 [#/sec] (mean)
    Time per request:       14.041 [ms] (mean)
    Time per request:       1.404 [ms] (mean, across all concurrent requests)


CI/CD
=====

The application is configured to use GitlabCI, and the pipeline configuration is describe in `.gitlab-ci.yml`

Stages:
  - setup
    - app
        - install dependencies
        - check dependencies for security audit
        - run unit and component tests
        - webpack build
    - server
        - install dependencies
        - check dependencies for security audit
        - run unit and component tests
        - tsc build
  - build
    - build the docker image
    - tag docker image with branch / git commit hash
    - push to container registru
  - test
    - run container security scanning
    - run smoke [TODO]
    - run e2e testing with puppeteer or playwright [TODO]
  - deploy
    - master only
    - deploy to GCP or AWS [TODO]





General Thought, Assumptions and Ramblings
==========================================

Local development:
I've chosen to use Docker and Docker-compose to run local development.  This facilitates run a production like environment on a local developer machine.
It also enables local testing of the build process and artifacts, which is automated in ci.

Assuming your production deployment is to Kubernetes environment, an alternative to docker-compose would be something
like microK8 [https://microk8s.io/] for local development.

Loadbalancer => Server => Storage

For a loadbalancer, I'm using Nginx [https://www.nginx.com/] which is the default loadbalancer in kubernetes.
Using nginx in development allows for adding ssl certificates and configuring local domain names, which could further
simulate real production requirements. Using a loadbalancer allows us to simulate ssl termintation, caching and other production
feature in a local development environmnet.

For storage, I'm using Redis [https://redis.io/]. Redis is a high performance key/value memory store. It can be configured
to persist to disk (either by AOF or RDS configurations [https://redis.io/topics/persistence]). Due to the nature of the assignemnt
a key/value store is ideal for storing slug/url combinations.

Production services:
Nginx => Node / Express => Redis (with RDS persistant configuration)

Local development services:
Nginx => Node / Express (ts-node-dev) [live reload]       => Redis
      => Node / webpack-dev-server / React [live-reload]


12factor App
============

Environment variables [https://12factor.net/config]:
Application config which varies between environments is stored seperately for the code, and injected at run time using environment variables.
We use a .env file located here `.env/server.env` which is used in conjunction with docker-compose.  A local copy can be made and used by overriding the docker-compse.yml file with a docker-compose.override.yml file.

Dependencies [https://12factor.net/dependencies]:
Using npm and package-lock.json file to explicitly declare deps.
Also note the we use the `--exact` flag for npm installing and there should be no `^` loose version numbers declared in the package.json file either.
This includes Docker images where we prefer the complete explicit image tag, rather than the looser version or "LATEST" tag.

Backing services [https://12factor.net/backing-services]:
All services are linked within the docker network and resolve on container_name.
The server implement re-connect strategy and has been tested to be tolerant to independantly bringing down the redis container and back up again.

Build Release Run [https://12factor.net/build-release-run]:
Code is build and dockerised into a container in the Gitlab CI pipeline. Each container is tag with a version (git hash) to identify the release.
Release containers can be independantly deployed to environments, and use the environment config file.

Dev Prod Parity [https://12factor.net/dev-prod-parity]:
By using a docker-compose, loadbalancer and redis containers, we are able to simulate a prod like environment locally.



Dependencies and Libraries
==========================

App
---
React =>
Forms => simple use case, so using a custom built form using hooks which keeps the bundle size small
    optional for more complex/dynamic forms Formik [https://formik.org/] could be a good alternative
Styling => use styled-component [https://styled-components.com/]
    optional to include a utility-first css framework like tailwind [https://tailwindcss.com/] and/or combined approach by using twin.macro [https://github.com/ben-rogerson/twin.macro]
    see blog post [https://mxstbr.com/thoughts/tailwind/] by Max Strober behind styled-components
Routing => simple use case, so not needed as yet
    optional for a bigger application something like react-router [https://reactrouter.com/] could be a good option
Types => using Typescript [https://www.typescriptlang.org/] which helps document code, catch silly bugs and reduces need to write bad argument typeguards and test cases
Code style => using eslint [https://eslint.org/] and prettier [https://prettier.io/]  to automate consistancy of code styling, making it easier for many people to work in same repo with neutral standards
    prettier has code integration with VSCode and the repo is setup to configure "Format on Save"
Build => using custom babel [https://babeljs.io/] and webpack [https://webpack.js.org/] setup
    Whilst using something like create-react-app would be faster and easier to get started with,
    it comes with a full batteries included which could end up being overkill or bloated in comparison to needs.
    Implementing the individual pieces, means it takes a bit longer to setup, but your understanding of the pieces is
    more complete and allows you to tailor, tweak, and swap out pieces to meet your requirement as your project grows.
    Optionally evaluate if using esbuild [https://esbuild.github.io/] would producer faster builds, smaller or same bundle sizes, and have less downstream dependencies to manage
Testing => jest, @testing-library


Server
------
Express => stable server framework for node
Types => using Typescript [https://www.typescriptlang.org/] which helps document code, catch silly bugs and reduces need to write bad argument typeguards and test cases
Code style => using eslint [https://eslint.org/] and prettier [https://prettier.io/]  to automate consistancy of code styling, making it easier for many people to work in same repo with neutral standards
    prettier has code integration with VSCode and the repo is setup to configure "Format on Save"
Testing => jest, @testing-library, supertest
