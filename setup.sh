#!/bin/sh

# install app dependancies
docker-compose run --rm --no-dep app npm i

# install server dependancies
docker-compose run --rm --no-dep server npm i
