#!/bin/bash
echo '{"url":"https://domain.com/very/long/url"}' > payload.json
ab -p payload.json -T application/json -c 10 -n 2000 http://localhost:8080/v1/api/generate
rm payload.json
