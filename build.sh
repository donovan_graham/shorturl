#!/bin/sh

docker-compose run --rm --no-dep app npm run build
docker-compose run --rm --no-dep server npm run build

# clean build dir
git clean -fdx build

# copy files
mv server/dist build/server
cp -r server/package.json server/package-lock.json server/.npmrc server/src/static build/server/
mv app/dist build/server/app
cd build

# build and tag image
docker build --no-cache --rm --tag shorturl:rc-1 .

# clean up
docker image prune -f

